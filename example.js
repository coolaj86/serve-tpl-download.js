'use strict';

var path = require('path');
var express = require('express');
var app = express();
//var serveTpl = require('serve-tpl-attachment');
var serveTpl = require('./');
var serveIndex = require('serve-index')(__dirname, { template: serveTpl() });

app.use('/', function (req, res, next) {
  // enable direct downloads for express.static()
  if (req.query.download) {
    res.setHeader('Content-Type', 'application/octet-stream');
    res.setHeader('Content-Disposition', 'attachment; filename="'+path.basename(req.url.replace(/\?.*/, ''))+'"');
  }
  next();
}, express.static(__dirname), serveIndex);

module.exports = app;
if (module === require.main) {
  require('http').createServer(app).listen(4080, function () { console.log(this.address()); });
}
