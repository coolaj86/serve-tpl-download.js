serve-tpl-attachment
==================

```js
var serveIndex = require('serve-index')({
  template: require('serve-tpl-attachment')()
});
```

A fork of the original `serve-index` template that, in combination with `serve-static`,
provides support for direct file downloads (using the Content-Disposition attachment header).

### Example Usage

```js
var serveTpl = require('serve-tpl-attachment');
var serveIndex = require('serve-index')('./public', { template: serveTpl() });

app.use('/', function (req, res, next) {
  // enable direct downloads for express.static()
  if (req.query.download) {
    res.setHeader('Content-Type', 'application/octet-stream');
    res.setHeader('Content-Disposition', 'attachment; filename="'+
      path.basename(req.url.replace(/\?.*/, ''))
    +'"');
  }
  next();
}, express.static('./public'), serveIndex);
```

Additional Options
==================

### privatefiles

As an additional security precaution you can ignore files which are not world-readable.

For example, this would prevent files in a `~/.ssh` from being read even when `dotfiles` are allowed.

`{ privatefiles: 'ignore' }`

```js
var serveTpl = require('serve-tpl-attachment');

var serveTemplate = serveTpl({ privatefiles: 'ignore' })
```

This is most effective on Unix-based systems (macOS, Linux, Android).
Windows may rely on ACLs instead of user-group-other style permissions.
